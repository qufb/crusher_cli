#!/usr/bin/env python3

import argparse
import crusher
import sys

parser = argparse.ArgumentParser()
parser.add_argument("dll", type=str, help="Path for CrusherS.dll")
parser.add_argument("data", type=str, help="Path for data file")
parser.add_argument("-c", "--compress", action="store_true", help="Compress data")
parser.add_argument("-d", "--decompress", type=int, help="Decompress data, using given output size in bytes")
parser.add_argument("-o", "--output-file", type=str, help="Output file for processed data")
parsed_args = parser.parse_args()

dll_path = parsed_args.dll
data_path = parsed_args.data

with open(data_path, "rb") as f:
    data = f.read()
crusher.init(dll_path)

if parsed_args.compress:
    output = crusher.compress(data)
elif parsed_args.decompress:
    output_size = parsed_args.decompress
    output = crusher.expand(data, output_size)
else:
    parser.print_help(sys.stderr)
    sys.exit(1)

if parsed_args.output_file:
    with open(parsed_args.output_file.strip(), "wb+") as f:
        f.write(output)
else:
    sys.stdout.buffer.write(output)
