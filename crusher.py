#!/usr/bin/env python3


"""

Interface to the long-defunct Crusher! compression library by David Cecil.

Initialize with the path to crushers.dll:

    crusher.init(dllpath)

Expand from a bytes object to another bytes object of specified size:

    result = crusher.expand(compressed_data, output_size)

Errors from crushers.dll will generally raise crusher.error.  However,
incorrect parameters to crusher.expand may cause access violations.

"""

import atexit
import ctypes

ERROR_FORMAT = "{}{} failed with error code {}"
UNCOMPRESSABLE_FORMAT = "{}{} failed with error code {} (uncompressable)"


class error(Exception):
    def __init__(self, msg):
        self.msg = msg


def _cxTry(func, *args):
    rc = func(*args)
    if rc != 0:
        raise error(ERROR_FORMAT.format(func.__name__, args, rc))


def _cxTryCompress(func, *args):
    rc = func(*args)
    if rc < 0:
        raise error(UNCOMPRESSABLE_FORMAT.format(func.__name__, args, rc))
    return rc


def compress(data):
    _cxTry(_crusherdll.cxBuf2BufInit)
    outbuf = ctypes.create_string_buffer(len(data))
    try:
        outlen = _cxTryCompress(_compressor, data, outbuf, len(data))
    finally:
        _cxTry(_crusherdll.cxBuf2BufClose)
    return outbuf.raw[:outlen]


def expand(data, outputsize):
    _cxTry(_crusherdll.cxBuf2BufInit)
    outbuf = ctypes.create_string_buffer(outputsize)
    try:
        _cxTry(_expander, data, outbuf, outputsize, len(data))
    finally:
        _cxTry(_crusherdll.cxBuf2BufClose)
    return outbuf.raw


_crusherdll = None
_compressor = None
_expander = None


def init(dllpath):
    global _crusherdll
    global _compressor
    global _expander
    _crusherdll = ctypes.WinDLL(dllpath)
    _cxTry(_crusherdll.cxInit)
    _compressor = _crusherdll.cxBuf2BufCompress
    _compressor.argtypes = (
        ctypes.c_char_p,
        ctypes.c_char_p,
        ctypes.c_uint,
    )
    _expander = _crusherdll.cxBuf2BufExpand
    _expander.argtypes = (
        ctypes.c_char_p,
        ctypes.c_char_p,
        ctypes.c_ulong,
        ctypes.c_ulong,
    )
    atexit.register(_close)


def _close():
    _cxTry(_crusherdll.cxCleanup)
