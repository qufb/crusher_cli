# crusher_cli

CLI for interoperability with the `Crusher!` compression format.

### Usage

Tested under Linux with `wine-6.0-rc6` and `python-3.7.4.exe` (x86).

```
crusher_cli.py [-h] [-c] [-d DECOMPRESS] [-o OUTPUT_FILE] dll data

positional arguments:
  dll                   Path for CrusherS.dll
  data                  Path for data file

optional arguments:
  -h, --help            show this help message and exit
  -c, --compress        Compress data
  -d DECOMPRESS, --decompress DECOMPRESS
                        Decompress data, using given output size in bytes
  -o OUTPUT_FILE, --output-file OUTPUT_FILE
                        Output file for processed data
```

### Credits

- [Original script](https://forum.xentax.com/viewtopic.php?t=17247)
